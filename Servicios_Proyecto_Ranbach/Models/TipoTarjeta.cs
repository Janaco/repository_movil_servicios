﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class TipoTarjeta
    {
        public TipoTarjeta()
        {
            Tarjeta = new HashSet<Tarjeta>();
        }

        public int IdTipoTarjeta { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }

        public virtual ICollection<Tarjeta> Tarjeta { get; set; }
    }
}
