﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Parametro
    {
        public int IdParametro { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public string Estado { get; set; }
        public DateTime FecRegistro { get; set; }
        public DateTime FecModificacion { get; set; }
    }
}
