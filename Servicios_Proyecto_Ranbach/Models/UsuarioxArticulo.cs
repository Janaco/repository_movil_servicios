﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class UsuarioxArticulo
    {
        public int Id { get; set; }
        public int IdArticulo { get; set; }
        public int IdUsuario { get; set; }
        public int IdNivel { get; set; }
        public DateTime Fecha { get; set; }

        public virtual Articulo IdArticuloNavigation { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
