﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class TipoMoneda
    {
        public TipoMoneda()
        {
            Apuesta = new HashSet<Apuesta>();
        }

        public int IdTipoMoneda { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Apuesta> Apuesta { get; set; }
    }
}
