﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Tarjeta
    {
        public int IdTarjeta { get; set; }
        public int IdTipoTarjeta { get; set; }
        public string NumeroTarjeta { get; set; }
        public DateTime FecEmision { get; set; }
        public string Estado { get; set; }

        public virtual TipoTarjeta IdTipoTarjetaNavigation { get; set; }
    }
}
