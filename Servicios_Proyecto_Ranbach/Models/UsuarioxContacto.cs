﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class UsuarioxContacto
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdUsuarioxContacto { get; set; }
        public int Estado { get; set; }
        public DateTime? FecRegistro { get; set; }
        public DateTime? FecModificacion { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
