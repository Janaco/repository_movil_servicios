﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Nivel
    {
        public Nivel()
        {
            ArticuloxNivel = new HashSet<ArticuloxNivel>();
            UsuarioxNivel = new HashSet<UsuarioxNivel>();
        }

        public int IdNivel { get; set; }
        public string NroNivel { get; set; }
        public string Experiencia { get; set; }
        public DateTime? FecRegistro { get; set; }
        public DateTime? FecModificacion { get; set; }

        public virtual ICollection<ArticuloxNivel> ArticuloxNivel { get; set; }
        public virtual ICollection<UsuarioxNivel> UsuarioxNivel { get; set; }
    }
}
