﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Categoria
    {
        public Categoria()
        {
            Producto = new HashSet<Producto>();
        }

        public int IdCategoria { get; set; }
        public string Descripcion { get; set; }
        public int Estado { get; set; }
        public DateTime? FecRegistro { get; set; }
        public DateTime? FecModificacion { get; set; }

        public virtual ICollection<Producto> Producto { get; set; }
    }
}
