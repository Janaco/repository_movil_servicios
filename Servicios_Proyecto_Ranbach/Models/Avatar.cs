﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Avatar
    {
        public int IdAvatar { get; set; }
        public int IdUsuario { get; set; }
        public string RutaFoto { get; set; }
        public string RutaAvatar { get; set; }
        public DateTime FecRegistro { get; set; }
        public DateTime FecModificacion { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
