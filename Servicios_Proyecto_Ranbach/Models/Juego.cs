﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Juego
    {
        public int IdJuego { get; set; }
        public int IdApk { get; set; }
        public string Nombre { get; set; }
        public string Estado { get; set; }
        public DateTime FecRegistro { get; set; }
        public DateTime FecModificacion { get; set; }
    }
}
