﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class MisionxSemana
    {
        public int Id { get; set; }
        public int IdMision { get; set; }
        public string Semana { get; set; }

        public virtual Mision IdMisionNavigation { get; set; }
    }
}
