﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Producto
    {
        public int IdProducto { get; set; }
        public string Descripcion { get; set; }
        public string RutaFoto { get; set; }
        public int IdCategoria { get; set; }
        public DateTime FecRegistro { get; set; }
        public DateTime FecModificacion { get; set; }
        public string Token { get; set; }
        public string Estado { get; set; }

        public virtual Categoria IdCategoriaNavigation { get; set; }
    }
}
