﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Apuesta
    {
        public int IdApuesta { get; set; }
        public int IdTipoModena { get; set; }
        public string Monto { get; set; }

        public virtual TipoMoneda IdTipoModenaNavigation { get; set; }
    }
}
