﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class MisionxUsuario
    {
        public int Id { get; set; }
        public int IdMision { get; set; }
        public int IdUsuario { get; set; }
        public string EtapaAct { get; set; }
        public string EtapaMax { get; set; }
        public string FecRegistro { get; set; }
        public string FecModificacion { get; set; }

        public virtual Mision IdMisionNavigation { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
