﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class CofreUsuario
    {
        public int IdCofre { get; set; }
        public int IdUsuario { get; set; }
        public DateTime FecApertura { get; set; }
        public string Experiencia { get; set; }
        public string Token { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
