﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Contacto
    {
        public int IdContacto { get; set; }
        public int IdUsuario { get; set; }
        public int IdContactoxUsuario { get; set; }
        public string Estado { get; set; }
        public DateTime FecRegistro { get; set; }
        public DateTime FecModificacion { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
