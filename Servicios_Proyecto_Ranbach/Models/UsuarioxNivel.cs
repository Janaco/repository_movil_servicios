﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class UsuarioxNivel
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdNivel { get; set; }
        public string Experiencia { get; set; }
        public string Token { get; set; }

        public virtual Nivel IdNivelNavigation { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
