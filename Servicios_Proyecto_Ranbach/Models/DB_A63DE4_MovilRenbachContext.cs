﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class DB_A63DE4_MovilRenbachContext : DbContext
    {
        public DB_A63DE4_MovilRenbachContext()
        {
        }

        public DB_A63DE4_MovilRenbachContext(DbContextOptions<DB_A63DE4_MovilRenbachContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Apuesta> Apuesta { get; set; }
        public virtual DbSet<Articulo> Articulo { get; set; }
        public virtual DbSet<ArticuloxNivel> ArticuloxNivel { get; set; }
        public virtual DbSet<Avatar> Avatar { get; set; }
        public virtual DbSet<Categoria> Categoria { get; set; }
        public virtual DbSet<CofreUsuario> CofreUsuario { get; set; }
        public virtual DbSet<Juego> Juego { get; set; }
        public virtual DbSet<Mision> Mision { get; set; }
        public virtual DbSet<MisionxSemana> MisionxSemana { get; set; }
        public virtual DbSet<MisionxUsuario> MisionxUsuario { get; set; }
        public virtual DbSet<Nivel> Nivel { get; set; }
        public virtual DbSet<Parametro> Parametro { get; set; }
        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<Tarjeta> Tarjeta { get; set; }
        public virtual DbSet<TipoDocumento> TipoDocumento { get; set; }
        public virtual DbSet<TipoMoneda> TipoMoneda { get; set; }
        public virtual DbSet<TipoTarjeta> TipoTarjeta { get; set; }
        public virtual DbSet<Token> Token { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<UsuarioxArticulo> UsuarioxArticulo { get; set; }
        public virtual DbSet<UsuarioxContacto> UsuarioxContacto { get; set; }
        public virtual DbSet<UsuarioxNivel> UsuarioxNivel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=MovilRenbachDB");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Apuesta>(entity =>
            {
                entity.HasKey(e => e.IdApuesta);

                entity.Property(e => e.IdApuesta)
                    .HasColumnName("idApuesta")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdTipoModena).HasColumnName("idTipoModena");

                entity.Property(e => e.Monto)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.HasOne(d => d.IdTipoModenaNavigation)
                    .WithMany(p => p.Apuesta)
                    .HasForeignKey(d => d.IdTipoModena)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Apuesta_TipoMoneda");
            });

            modelBuilder.Entity<Articulo>(entity =>
            {
                entity.HasKey(e => e.IdArticulo);

                entity.Property(e => e.IdArticulo)
                    .HasColumnName("idArticulo")
                    .ValueGeneratedNever();

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(150);

                entity.Property(e => e.RutaArticulo)
                    .IsRequired()
                    .HasColumnName("rutaArticulo")
                    .HasMaxLength(150);

                entity.Property(e => e.TipoPase)
                    .IsRequired()
                    .HasColumnName("tipoPase")
                    .HasMaxLength(20)
                    .IsFixedLength();
            });

            modelBuilder.Entity<ArticuloxNivel>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.IdArticulo).HasColumnName("idArticulo");

                entity.Property(e => e.IdNivel).HasColumnName("idNivel");

                entity.HasOne(d => d.IdArticuloNavigation)
                    .WithMany(p => p.ArticuloxNivel)
                    .HasForeignKey(d => d.IdArticulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ArticuloxNivel_Articulo");

                entity.HasOne(d => d.IdNivelNavigation)
                    .WithMany(p => p.ArticuloxNivel)
                    .HasForeignKey(d => d.IdNivel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ArticuloxNivel_Nivel");
            });

            modelBuilder.Entity<Avatar>(entity =>
            {
                entity.HasKey(e => e.IdAvatar);

                entity.Property(e => e.IdAvatar).HasColumnName("idAvatar");

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.RutaAvatar)
                    .IsRequired()
                    .HasColumnName("rutaAvatar")
                    .HasMaxLength(150);

                entity.Property(e => e.RutaFoto)
                    .IsRequired()
                    .HasColumnName("rutaFoto")
                    .HasMaxLength(150);

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Avatar)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Avatar_Usuario");
            });

            modelBuilder.Entity<Categoria>(entity =>
            {
                entity.HasKey(e => e.IdCategoria)
                    .HasName("PK_Menu");

                entity.Property(e => e.IdCategoria).HasColumnName("idCategoria");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(150);

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<CofreUsuario>(entity =>
            {
                entity.HasKey(e => e.IdCofre);

                entity.Property(e => e.IdCofre)
                    .HasColumnName("idCofre")
                    .ValueGeneratedNever();

                entity.Property(e => e.Experiencia)
                    .IsRequired()
                    .HasColumnName("experiencia")
                    .HasMaxLength(150);

                entity.Property(e => e.FecApertura)
                    .HasColumnName("fecApertura")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnName("token")
                    .HasMaxLength(150);

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.CofreUsuario)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CofreUsuario_Usuario");
            });

            modelBuilder.Entity<Juego>(entity =>
            {
                entity.HasKey(e => e.IdJuego);

                entity.Property(e => e.IdJuego)
                    .HasColumnName("idJuego")
                    .ValueGeneratedNever();

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdApk).HasColumnName("idAPK");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<Mision>(entity =>
            {
                entity.HasKey(e => e.IdMision);

                entity.Property(e => e.IdMision)
                    .HasColumnName("idMision")
                    .ValueGeneratedNever();

                entity.Property(e => e.Detalle)
                    .IsRequired()
                    .HasColumnName("detalle")
                    .HasMaxLength(150);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasMaxLength(20);

                entity.Property(e => e.Etapa)
                    .IsRequired()
                    .HasColumnName("etapa")
                    .HasMaxLength(20);

                entity.Property(e => e.Experiencia)
                    .IsRequired()
                    .HasColumnName("experiencia")
                    .HasMaxLength(20);

                entity.Property(e => e.FecModificacion)
                    .IsRequired()
                    .HasColumnName("fecModificacion")
                    .HasMaxLength(150);

                entity.Property(e => e.FecRegistro)
                    .IsRequired()
                    .HasColumnName("fecRegistro")
                    .HasMaxLength(150);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<MisionxSemana>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdMision).HasColumnName("idMision");

                entity.Property(e => e.Semana)
                    .IsRequired()
                    .HasColumnName("semana")
                    .HasMaxLength(20);

                entity.HasOne(d => d.IdMisionNavigation)
                    .WithMany(p => p.MisionxSemana)
                    .HasForeignKey(d => d.IdMision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MisionxSemana_Mision");
            });

            modelBuilder.Entity<MisionxUsuario>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.EtapaAct)
                    .IsRequired()
                    .HasColumnName("etapaAct")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.EtapaMax)
                    .IsRequired()
                    .HasColumnName("etapaMax")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.FecModificacion)
                    .IsRequired()
                    .HasColumnName("fecModificacion")
                    .HasMaxLength(150);

                entity.Property(e => e.FecRegistro)
                    .IsRequired()
                    .HasColumnName("fecRegistro")
                    .HasMaxLength(150);

                entity.Property(e => e.IdMision).HasColumnName("idMision");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.HasOne(d => d.IdMisionNavigation)
                    .WithMany(p => p.MisionxUsuario)
                    .HasForeignKey(d => d.IdMision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MisionxUsuario_Mision");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.MisionxUsuario)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MisionxUsuario_Usuario");
            });

            modelBuilder.Entity<Nivel>(entity =>
            {
                entity.HasKey(e => e.IdNivel);

                entity.Property(e => e.IdNivel).HasColumnName("idNivel");

                entity.Property(e => e.Experiencia)
                    .IsRequired()
                    .HasColumnName("experiencia")
                    .HasMaxLength(20);

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NroNivel)
                    .IsRequired()
                    .HasColumnName("nroNivel")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Parametro>(entity =>
            {
                entity.HasKey(e => e.IdParametro);

                entity.Property(e => e.IdParametro)
                    .HasColumnName("idParametro")
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(150);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasMaxLength(50);

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasColumnName("valor")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.IdProducto);

                entity.Property(e => e.IdProducto).HasColumnName("idProducto");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(150);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasMaxLength(20);

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdCategoria).HasColumnName("idCategoria");

                entity.Property(e => e.RutaFoto)
                    .IsRequired()
                    .HasColumnName("rutaFoto")
                    .HasMaxLength(150);

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnName("token")
                    .HasMaxLength(20);

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.IdCategoria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Producto_Categoria");
            });

            modelBuilder.Entity<Tarjeta>(entity =>
            {
                entity.HasKey(e => e.IdTarjeta);

                entity.Property(e => e.IdTarjeta).HasColumnName("idTarjeta");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasMaxLength(20);

                entity.Property(e => e.FecEmision)
                    .HasColumnName("fecEmision")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdTipoTarjeta).HasColumnName("idTipoTarjeta");

                entity.Property(e => e.NumeroTarjeta)
                    .IsRequired()
                    .HasColumnName("numeroTarjeta")
                    .HasMaxLength(150);

                entity.HasOne(d => d.IdTipoTarjetaNavigation)
                    .WithMany(p => p.Tarjeta)
                    .HasForeignKey(d => d.IdTipoTarjeta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TipoTarjeta");
            });

            modelBuilder.Entity<TipoDocumento>(entity =>
            {
                entity.HasKey(e => e.IdTipoDocumento);

                entity.Property(e => e.IdTipoDocumento).HasColumnName("idTipoDocumento");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<TipoMoneda>(entity =>
            {
                entity.HasKey(e => e.IdTipoMoneda);

                entity.Property(e => e.IdTipoMoneda)
                    .HasColumnName("idTipoMoneda")
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<TipoTarjeta>(entity =>
            {
                entity.HasKey(e => e.IdTipoTarjeta);

                entity.Property(e => e.IdTipoTarjeta).HasColumnName("idTipoTarjeta");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(150);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasMaxLength(20)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.HasKey(e => e.IdToken);

                entity.Property(e => e.IdToken).HasColumnName("idToken");

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Monto)
                    .IsRequired()
                    .HasColumnName("monto")
                    .HasMaxLength(20);

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Token)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Token_Usuario");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Apellido)
                    .HasColumnName("apellido")
                    .HasMaxLength(150);

                entity.Property(e => e.Contrasenia)
                    .IsRequired()
                    .HasColumnName("contrasenia")
                    .HasMaxLength(150);

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasColumnName("correo")
                    .HasMaxLength(150);

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdTipoDocumento)
                    .HasColumnName("idTipoDocumento")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(150);

                entity.Property(e => e.NombreUsuario)
                    .IsRequired()
                    .HasColumnName("nombreUsuario")
                    .HasMaxLength(150);

                entity.Property(e => e.NroDocumento)
                    .IsRequired()
                    .HasColumnName("nroDocumento")
                    .HasMaxLength(20);

                entity.Property(e => e.TokenChat)
                    .HasColumnName("tokenChat")
                    .HasMaxLength(250);

                entity.HasOne(d => d.IdTipoDocumentoNavigation)
                    .WithMany(p => p.Usuario)
                    .HasForeignKey(d => d.IdTipoDocumento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario_TipoDocumento");
            });

            modelBuilder.Entity<UsuarioxArticulo>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdArticulo).HasColumnName("idArticulo");

                entity.Property(e => e.IdNivel).HasColumnName("idNivel");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.HasOne(d => d.IdArticuloNavigation)
                    .WithMany(p => p.UsuarioxArticulo)
                    .HasForeignKey(d => d.IdArticulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsuarioxArticulo_Articulo");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.UsuarioxArticulo)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsuarioxArticulo_Usuario");
            });

            modelBuilder.Entity<UsuarioxContacto>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FecModificacion)
                    .HasColumnName("fecModificacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.FecRegistro)
                    .HasColumnName("fecRegistro")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.IdUsuarioxContacto).HasColumnName("idUsuarioxContacto");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.UsuarioxContacto)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contacto_Usuario");
            });

            modelBuilder.Entity<UsuarioxNivel>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Experiencia)
                    .IsRequired()
                    .HasColumnName("experiencia")
                    .HasMaxLength(20)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IdNivel).HasColumnName("idNivel");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnName("token")
                    .HasMaxLength(20)
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.IdNivelNavigation)
                    .WithMany(p => p.UsuarioxNivel)
                    .HasForeignKey(d => d.IdNivel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Nivel");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.UsuarioxNivel)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
