﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class ArticuloxNivel
    {
        public int Id { get; set; }
        public int IdArticulo { get; set; }
        public int IdNivel { get; set; }
        public string Estado { get; set; }

        public virtual Articulo IdArticuloNavigation { get; set; }
        public virtual Nivel IdNivelNavigation { get; set; }
    }
}
