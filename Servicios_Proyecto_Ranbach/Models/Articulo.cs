﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Articulo
    {
        public Articulo()
        {
            ArticuloxNivel = new HashSet<ArticuloxNivel>();
            UsuarioxArticulo = new HashSet<UsuarioxArticulo>();
        }

        public int IdArticulo { get; set; }
        public string Nombre { get; set; }
        public string TipoPase { get; set; }
        public string RutaArticulo { get; set; }
        public DateTime FecRegistro { get; set; }
        public DateTime FecModificacion { get; set; }

        public virtual ICollection<ArticuloxNivel> ArticuloxNivel { get; set; }
        public virtual ICollection<UsuarioxArticulo> UsuarioxArticulo { get; set; }
    }
}
