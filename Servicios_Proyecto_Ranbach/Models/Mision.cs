﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Mision
    {
        public Mision()
        {
            MisionxSemana = new HashSet<MisionxSemana>();
            MisionxUsuario = new HashSet<MisionxUsuario>();
        }

        public int IdMision { get; set; }
        public string Nombre { get; set; }
        public string Detalle { get; set; }
        public string Experiencia { get; set; }
        public string Etapa { get; set; }
        public string Estado { get; set; }
        public string FecRegistro { get; set; }
        public string FecModificacion { get; set; }

        public virtual ICollection<MisionxSemana> MisionxSemana { get; set; }
        public virtual ICollection<MisionxUsuario> MisionxUsuario { get; set; }
    }
}
