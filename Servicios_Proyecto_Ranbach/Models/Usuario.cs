﻿using System;
using System.Collections.Generic;

namespace Servicios_Proyecto_Ranbach.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            Avatar = new HashSet<Avatar>();
            CofreUsuario = new HashSet<CofreUsuario>();
            MisionxUsuario = new HashSet<MisionxUsuario>();
            Token = new HashSet<Token>();
            UsuarioxArticulo = new HashSet<UsuarioxArticulo>();
            UsuarioxContacto = new HashSet<UsuarioxContacto>();
            UsuarioxNivel = new HashSet<UsuarioxNivel>();
        }

        public int IdUsuario { get; set; }
        public int IdTipoDocumento { get; set; }
        public string NombreUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NroDocumento { get; set; }
        public string Correo { get; set; }
        public string Contrasenia { get; set; }
        public DateTime? FecRegistro { get; set; }
        public DateTime? FecModificacion { get; set; }
        public string TokenChat { get; set; }
        public int Estado { get; set; }

        public virtual TipoDocumento IdTipoDocumentoNavigation { get; set; }
        public virtual ICollection<Avatar> Avatar { get; set; }
        public virtual ICollection<CofreUsuario> CofreUsuario { get; set; }
        public virtual ICollection<MisionxUsuario> MisionxUsuario { get; set; }
        public virtual ICollection<Token> Token { get; set; }
        public virtual ICollection<UsuarioxArticulo> UsuarioxArticulo { get; set; }
        public virtual ICollection<UsuarioxContacto> UsuarioxContacto { get; set; }
        public virtual ICollection<UsuarioxNivel> UsuarioxNivel { get; set; }
    }
}
