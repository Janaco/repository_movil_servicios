﻿using Servicios_Proyecto_Ranbach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicios_Proyecto_Ranbach.CustomModels
{
    public class RespuestaUsuarioModel: RespuestaModel
    {
        public Usuario usuario { get; set; }
        public Nivel nivel { get; set; }
        public UsuarioxNivel userxnivel { get; set; }
    }
}
