﻿using Servicios_Proyecto_Ranbach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicios_Proyecto_Ranbach.CustomModels
{
    public class CustomUsuarioxContacto
    {

        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdUsuarioxContacto { get; set; }
        public string nombreContacto { get; set; }
        public int Estado { get; set; }


        public CustomUsuarioxContacto(UsuarioxContacto obj)
        {

            this.Id = obj.Id;
            this.IdUsuario = obj.IdUsuario;
            this.IdUsuarioxContacto = obj.IdUsuarioxContacto;
            this.Estado = obj.Estado;

        }

    }
}
