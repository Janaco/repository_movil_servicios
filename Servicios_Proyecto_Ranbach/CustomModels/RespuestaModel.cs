﻿using Servicios_Proyecto_Ranbach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicios_Proyecto_Ranbach.CustomModels
{
    public class RespuestaModel
    {
        public bool Success { get; set; }
        public int codigo { get; set; }        
        public string Mensaje { get; set; }

    }
}
