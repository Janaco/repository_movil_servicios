﻿using Servicios_Proyecto_Ranbach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicios_Proyecto_Ranbach.CustomModels
{
    public class UserWithToken : Usuario
    {
        public string TokenChat { get; set; }
       //public string RefreshToken { get; set; }

        public UserWithToken(Usuario user)
        {
            this.IdUsuario = user.IdUsuario;
            this.IdTipoDocumento = user.IdTipoDocumento;
            this.NombreUsuario = user.NombreUsuario;
            this.Nombre = user.Nombre;
            this.Apellido = user.Apellido;
            this.NroDocumento = user.NroDocumento;
            this.Contrasenia = user.Contrasenia;
            this.FecRegistro = user.FecRegistro;
            this.FecModificacion = user.FecModificacion;
            this.Estado = user.Estado;
        }
    }
}
