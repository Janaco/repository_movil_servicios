﻿using Servicios_Proyecto_Ranbach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicios_Proyecto_Ranbach.CustomModels
{
    public class RespuestaContactoModel: RespuestaModel
    {
        public UsuarioxContacto contacto { get; set; }
        public List<CustomUsuarioxContacto> listaContacto { get; set; }


    }
}
