﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Servicios_Proyecto_Ranbach.CustomModels;
using Servicios_Proyecto_Ranbach.Models;

namespace Servicios_Proyecto_Ranbach.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactosController : ControllerBase
    {
        private readonly DB_A63DE4_MovilRenbachContext _context;

        public ContactosController(DB_A63DE4_MovilRenbachContext context)
        {
            _context = context;
        }

        // GET: api/Contactos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UsuarioxContacto>>> GetUsuarioxContacto()
        {
            return await _context.UsuarioxContacto.ToListAsync();
        }

        // GET: api/Contactos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UsuarioxContacto>> GetUsuarioxContacto(int id)
        {
            var usuarioxContacto = await _context.UsuarioxContacto.FindAsync(id);

            if (usuarioxContacto == null)
            {
                return NotFound();
            }

            return usuarioxContacto;
        }

        // GET: api/Contactos/5
        [HttpGet("getContactosxUsuario/{idUser}")]
        //public async Task<ActionResult<IEnumerable<UsuarioxContacto>>> GetContactosxUsuario(int idUser)
        public async Task<IActionResult> GetContactosxUsuario(int idUser)
        {
            //var lista = await _context.UsuarioxContacto.Where(user => user.IdUsuario == idUser).ToListAsync();           
            
            var lista =  await (from contacto in _context.UsuarioxContacto
                                join user in _context.Usuario on contacto.IdUsuarioxContacto equals user.IdUsuario
                                where contacto.IdUsuario.Equals(idUser)
                                select new CustomUsuarioxContacto(contacto) { nombreContacto=user.NombreUsuario }) .ToListAsync();
            
            var success = construirRespuesta(success: true, codigo: 0, inLista: lista, mensaje: "Datos correctos");
            return Ok(success);
        }

        // PUT: api/Contactos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuarioxContacto(int id, UsuarioxContacto usuarioxContacto)
        {
            if (id != usuarioxContacto.Id)
            {
                return BadRequest();
            }

            _context.Entry(usuarioxContacto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioxContactoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Contactos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<UsuarioxContacto>> PostUsuarioxContacto(UsuarioxContacto usuarioxContacto)
        {
            try
            {
                _context.UsuarioxContacto.Add(usuarioxContacto);
                await _context.SaveChangesAsync();

                var success = construirRespuesta(success: true, codigo: 0, obj: usuarioxContacto, mensaje: "Datos correctos");
                return Ok(success);

            }
            catch (Exception e)
            {
                var error = construirRespuesta(success: false, codigo: 3, obj: null,
                    mensaje: e.Message + e.InnerException?.Message);
                return Ok(error);
            }

        }


        private RespuestaContactoModel construirRespuesta(bool success, string mensaje, int codigo, UsuarioxContacto obj=null, List<CustomUsuarioxContacto> inLista =null)
        {
            RespuestaContactoModel respuesta = new RespuestaContactoModel()
            {
                Success = success,
                Mensaje = mensaje,
                codigo = codigo,
                contacto = obj,
                listaContacto = inLista
            };
            return respuesta;
        }

        // DELETE: api/Contactos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UsuarioxContacto>> DeleteUsuarioxContacto(int id)
        {
            var usuarioxContacto = await _context.UsuarioxContacto.FindAsync(id);
            if (usuarioxContacto == null)
            {
                return NotFound();
            }

            _context.UsuarioxContacto.Remove(usuarioxContacto);
            await _context.SaveChangesAsync();

            return usuarioxContacto;
        }

        private bool UsuarioxContactoExists(int id)
        {
            return _context.UsuarioxContacto.Any(e => e.Id == id);
        }
    }
}
