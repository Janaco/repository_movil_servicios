﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Servicios_Proyecto_Ranbach.CustomModels;
using Servicios_Proyecto_Ranbach.Models;

namespace Servicios_Proyecto_Ranbach.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly DB_A63DE4_MovilRenbachContext _context;
        private readonly JWTSettings _jwtsettings;


        public UsuariosController(DB_A63DE4_MovilRenbachContext context, IOptions<JWTSettings> jwtsettings)
        {
            _context = context;
            _jwtsettings = jwtsettings.Value;
        }

        //// GET: api/Usuarios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Usuario>>> GetUsuario()
        {
            return await _context.Usuario.ToListAsync();
        }

        // GET: api/Usuarios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuario>> GetUsuario(int id)
        {
            var usuario = await _context.Usuario.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return usuario;
        }

        // GET: api/Usuarios/
        [HttpGet("getUserxNombreUser/{nombreUser}")]
        public async Task<ActionResult<Usuario>> GetUserxNombreUser(string nombreUser)
        {            
            var usuario = await _context.Usuario.Where(user => user.NombreUsuario == nombreUser).SingleOrDefaultAsync();

            if (usuario == null)
            {
                var error = construirRespuesta(false, "No se encontró al Usuario", 1, null);
                return Ok(error);
            }

            var success = construirRespuesta(success: true, codigo: 0, user: usuario,
              mensaje: "Datos correctos");
            return Ok(success);
        }


       
        // GET: api/usuarios/login
        [HttpPost("login")]
        public async Task<ActionResult<Usuario>> Login(Usuario inUser)
        {
            try
            {
                //var userExiste = await _context.Usuario.Where(user => user.NombreUsuario == inUser.NombreUsuario).SingleOrDefaultAsync();
                var userExiste = await _context.Usuario.Where(user => user.Correo == inUser.Correo).SingleOrDefaultAsync();

                if (userExiste == null)
                {
                    var error = construirRespuesta(false, "Correo incorrecto", 2, null);
                    return Ok(error);
                }

                Usuario usuarioLogin;

                if (inUser.Estado==2) // INDICA VIENE DE FACEBOOK O GOOGLE
                {
                    usuarioLogin = userExiste;
                }
                else
                {
                    usuarioLogin = await _context.Usuario.Where(user => user.NombreUsuario == userExiste.NombreUsuario && user.Contrasenia == inUser.Contrasenia).FirstOrDefaultAsync();
                    if (usuarioLogin == null)
                    {
                        var error = construirRespuesta(false, "Contraseña incorrecta", 2, null);
                        return Ok(error);
                    }
                }

                //INSERTO EL TOKEN CHAT
                usuarioLogin.TokenChat = GenerateAccessToken(usuarioLogin.IdUsuario);
                _context.Entry(usuarioLogin).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                Nivel nivel = new Nivel()
                {
                    NroNivel = "0"
                };

                UsuarioxNivel userxnivel = new UsuarioxNivel()
                {
                    Experiencia = "0",
                    Token = "0"
                };

                var success = construirRespuesta(true, "Datos correctos", 0, usuarioLogin, nivel, userxnivel);
                return Ok(success);
            }
            catch (Exception e)
            {
                var error = construirRespuesta(success: false, codigo: 3, user: null,
                   mensaje: e.Message + e.InnerException?.Message);
                return Ok(error);
            }

        }

        // PUT: api/Usuarios/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuario(int id, Usuario usuario)
        {
            try
            {
                // SE CAE CUANDO UN USUARIO TIENE MUCHAS CUENTAS Y EL DNI SÍ SE VA A REPETIR
                //var dniExiste = await _context.Usuario.Where(user => user.NroDocumento == usuario.NroDocumento).ToListAsync();

                //if (dniExiste.Count>1)
                //{
                //    var error = construirRespuesta(false, "Dni ya existe. Verifique Datos correctos", 1, null);
                //    return Ok(error);
                //}

                _context.Entry(usuario).State = EntityState.Modified;
                await _context.SaveChangesAsync();

               
                var success = construirRespuesta(success: true, codigo: 0, user: usuario,
                  mensaje: "Datos correctos");

                return Ok(success);
            }
            catch (Exception e)
            {
                var error = construirRespuesta(success: false, codigo: 3, user: null,
                   mensaje: e.Message + e.InnerException?.Message);
                return Ok(error);
            }

        }

        // POST: api/Usuarios
        [HttpPost]
        public async Task<ActionResult<Usuario>> PostUsuario(Usuario usuario)
        {
            try
            {                
                var usuarioExiste = await _context.Usuario.Where(user => user.NombreUsuario == usuario.NombreUsuario ).SingleOrDefaultAsync();
                if (usuarioExiste != null)
                {
                    var error = construirRespuesta(success: false, codigo: 1, user: null,
                                        mensaje: "Nombre de Usuario ya existe");
                    return Ok(error);
                }

                var correoExiste = await _context.Usuario.Where(user => user.Correo == usuario.Correo).SingleOrDefaultAsync();
                if (correoExiste != null)
                {
                    var error = construirRespuesta(success: false, codigo: 1, user: null,
                                        mensaje: "Correo ya existe");
                    return Ok(error);
                }

                _context.Usuario.Add(usuario);
                await _context.SaveChangesAsync();

                var success = construirRespuesta(success: true, codigo: 0, user: usuario,
                  mensaje: "Datos correctos");
                return Ok(success);
            }
            catch (Exception e)
            {
                var error = construirRespuesta(success:false,codigo:3,user:null,
                    mensaje: e.Message + e.InnerException?.Message);
                return Ok(error);
            }
           
        }


        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Usuario>> DeleteUsuario(int id)
        {
            var usuario = await _context.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            _context.Usuario.Remove(usuario);
            await _context.SaveChangesAsync();

            return usuario;
        }


        private RespuestaUsuarioModel construirRespuesta(bool success, string mensaje, int codigo, Usuario user, Nivel nivel = null, UsuarioxNivel userxnivel = null)
        {
            RespuestaUsuarioModel respuesta = new RespuestaUsuarioModel()
            {
                Success = success,
                Mensaje = mensaje,
                codigo = codigo,
                usuario = user,
                nivel = nivel,
                userxnivel = userxnivel

            };
            return respuesta;
        }

        private string GenerateAccessToken(int userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtsettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                        new Claim(ClaimTypes.Name, Convert.ToString(userId))
                }),
                Expires = DateTime.UtcNow.AddMonths(8),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
