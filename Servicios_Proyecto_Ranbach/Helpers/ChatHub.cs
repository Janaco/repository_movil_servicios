﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servicios_Proyecto_Ranbach.Helpers
{
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class ChatHub:Hub
    {
        //public async Task SendMessage(string user, string message)
        //{
        //    await Clients.All.SendAsync("ReceiveMessage", user, message);
        //}

        //public override async Task OnConnectedAsync()
        //{
        //    var user = Context.UserIdentifier;
        //    await Clients.All.SendAsync("ReceiveMessage", "conectado", user == null ? Context.ConnectionId : user);
        //    await base.OnConnectedAsync();
        //}

        public async Task SendPrivateMessage(string user, string message)
        {
            await Clients.User(user).SendAsync("ReceivePrivateMessage", message);
        }

    }
}
